# Cursorの設定

## Cursor

概要は下記個人ブログに詳しい．

- https://rakuraku-engineer.com/posts/cursor/


## インストール

下記公式サイトから実行ファイルを入手．

- https://cursor.sh/


## Docs

OpenFOAMの利用のため，下記ドキュメントを学習させた．

- https://snl-dakota.github.io/docs/6.19.0/                             "Dakota619"
- https://github.com/snl-dakota/dakota-examples/                        "DakotaEx"
- https://wiki.freecad.org/                                             "FCWiki" 
- https://www.openfoam.com/documentation/                               "OFDocs"
- https://develop.openfoam.com/Development/openfoam/-/tree/develop/     "OFRepo"
- https://develop.openfoam.com/Development/openfoam/-/wikis/            "OFCodeWiki"
- https://doc.openfoam.com/2312/                                        "OFDoc2312"
- https://wiki.openfoam.com/                                            "OFTutoWiki"     
- https://doc.cfd.direct/notes/cfd-general-principles/                  "OFNotes"
- https://www.cfdsupport.com/OpenFOAM-Training-by-CFD-Support/          "OFCfdSupport"
- https://holzmann-cfd.com/community/publications/                      "OFHoltzman"          

